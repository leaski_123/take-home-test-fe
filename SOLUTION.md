# SOLUTION
========

## Estimation
----------
Estimated: 2-2.5 hours
2 hours for the code, half an hour for reading and setting up codebase

Spent: 2hours 45 mins

## Solution
--------
Pre : Assumptions were made based on the product requirements and data set.

Initial splitting of tasks into:
1. Create a page / add it to index page navigation
2. Implement fetching data and displaying it, including discount(assumption)
3. Add pagination to fetched data
4. Add tags based on config
5. Add subscription filter - and filter out based on discounted value

## Test Cases
--------
Pagination:
* See pagination when there are more than 12 items in total
* Don't see pagination when there are 12 or fewer items
* Any refetch of data should reset the pagination back to the first page

Filtering:
* Price input
    * Inserting text should display an error to the user
    * the correct filtering is applied
* Combination, when multiple filters are combined the data returned should be correct (although I would say this is an API test)
* Multiple tags - should apply filtering and return items where all the tags are true - api doesn't work with this but would be a good addition

Navigation:
* navigating to existing pages works
* navigating to a page that doesn't exist takes you to an error page

## Comments
--------
I made a mistake that killed the server but kept the port busy, took a few mins to kill and restart.
I made a HUGE assumption on how the subscription and price filters work together
I made a decision not to display the results as a table, I don't think it's an appropriate way to display this type of information and would push back on criteria to do it this way without a good reason (printability etc.).

## Issues encountered
* missing dev dependency on the api server

## Further improvements (given time and weighing up their value)
--------
1. Use yarn - wasn't worth it since I am the only person working on the repo
2. Use Typescript for better type safety and quality - didn't implement due to time constraints
3. Caching the API results so the viewer doesn't need to refetch every time
    * pre-fetching potentially
4. Using frontend filtering rather than BE logic (if the data set is small enough and users can handle the size of it)
5. Add sorting to the results like a-z, price asc/desc
6. Styling
7. Translations for strings
8. page results as query params so that you can share/bookmark a page with results
9. Move the product display into its own component for reusability - only needed it in one place for now
10. improve display on multiple class names through a library or using styled components
11. Add a "not subscription" filter for subscriptions, currently it only handles true or doesn't apply the filter to the URL params
13. Add a price slider for range filtering
12. A proper loading state
13. Move some of the functions into utility files
14. Clean up the hook that fetched the data so that it is easier to read
15. Improve the style - did enough to make it easy to view but it's far from pretty!
16. Could add styled components - personal preference from me in using a plain SCSS approach
17. Add a no results view
18. Price input validation
19. Add links to the products - didn't feel it added to the value of the test