import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "./App.scss";
import { HomePage } from "./pages/HomePage";
import { ProductCollectionPage } from "./pages/ProductCollection/ProductCollection";

export default function App() {
  return (
    <div className="page">
      <div className="page__content">
        <Router>
          <Routes>
            <Route
              path="product-collection"
              element={<ProductCollectionPage />}
            />
            <Route index element={<HomePage />} />
          </Routes>
        </Router>
      </div>
    </div>
  );
}
