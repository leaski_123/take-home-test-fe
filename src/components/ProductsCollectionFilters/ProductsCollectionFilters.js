import React, { useState } from "react";
import { config } from "../../config";
import "./styles.scss";

export const ProductsCollectionFilters = ({ filters, setFilter }) => {
  const [priceInput, setPriceInput] = useState("");
  return (
    <div className="filters">
      <div>
        <h4>Tags</h4>
        <ul>
          {config.filters.tags.map((tag) => (
            <li key={tag}>
              <input
                type="checkbox"
                id={tag}
                checked={filters.tags[tag]}
                onChange={() => setFilter("tag", tag)}
              />
              <label htmlFor={tag}>{tag}</label>
            </li>
          ))}
        </ul>

        <h4>Subscription</h4>
        <ul>
          <li>
            <input
              type="checkbox"
              id="is_subscription"
              checked={filters.subscription}
              onChange={() => setFilter("subscription")}
            />
            <label htmlFor={"is_subscription"}>Subscription</label>
          </li>
        </ul>

        <h4>Price</h4>
        <ul>
          <li>
            <input
              type="text"
              id="price"
              onChange={(event) => {
                setPriceInput(event.target.value);
                setFilter("price", priceInput);
              }}
              value={priceInput}
            />
          </li>
        </ul>
      </div>
    </div>
  );
};
