import React from "react";
import "./styles.scss";

export const ProductsCollectionPagination = ({
  totalPages,
  currentPage = 1,
  setPage,
}) => {
  const pagesArray = Array.apply(null, Array(totalPages));
  if (totalPages === 1) {
    return <></>;
  }
  return (
    <div className="pagination">
      {pagesArray.map((item, index) => (
        <button
        key={`pagination-${index}`}
          className={
            index + 1 === currentPage
              ? "pagination-page pagination-page--active"
              : "pagination-page"
          }
          type="button"
          onClick={() => setPage(index + 1)}
        >
          {`Page ${index + 1}`}
        </button>
      ))}
    </div>
  );
};
