import React from "react";
import "./styles.scss";

export const ProductsCollectionResults = ({ results }) => {
  return (
    <>
      <div>
        {results.map((item) => (
          <div className="product-item" key={item.id}>
            <div className="product-item__image">
              <img src={item.image_src} alt="product" />
            </div>
            <div className="product-item__details">
              <h3 className="product-item__title">{item.title}</h3>
              <p>Price: £{item.price}</p>
              <p>
                {item.tags.map((tag) => (
                  <span className="product-item__tag" key={`${item.id}-${tag}`}>{tag}</span>
                ))}
              </p>

              {item.subscription && (
                <p>
                  Discount applied when bought as a subscription : £{item.subscription_discount}
                </p>
              )}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};
