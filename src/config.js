export const config = {
  resultsPerPage: 12,
  productUrl: "http://localhost:3010/products",
  filters: {
    tags: ["Formula", "Chews", "Dog", "Cat", "Shampoo"],
  },
};
