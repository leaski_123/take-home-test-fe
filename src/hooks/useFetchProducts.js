import { useEffect, useState } from "react";
import axios from "axios";
import { config } from "../config";

const convertToPrice = (priceString, additionalDiscount = 0) => {
  return (parseInt(priceString) + additionalDiscount).toFixed(2);
};

export const useFetchProducts = () => {
  const [filteredResults, setFilteredResults] = useState([]);
  const [page, setPage] = useState(1); // starts a 1 not 0
  const [totalResults, setTotalResults] = useState(1); // starts a 1 not 0
  const [totalPages, setTotalPages] = useState(1);
  const [filters, setFilters] = useState({ tags: {}, subscription: undefined });

  useEffect(() => {
    const fetchData = () => {
      const priceValue = filters.subscription ? convertToPrice(filters.price, 25) : convertToPrice(filters.price);
      const searchParams = new URLSearchParams({
        _page: page,
        _limit: config.resultsPerPage,
        ...(filters.tags && {tags_like: Object.keys(filters.tags)}),
        ...(filters.subscription && { subscription: filters.subscription }),
        ...(filters.price && filters.price.length && { price_lte: priceValue }),
      });

      axios(`${config.productUrl}?${searchParams}`).then(
        ({ data, headers }) => {
          setPage(1);
          const totalCount = headers["x-total-count"];
          const pagesForCount = totalCount > 12 ? (totalCount % 12) + 1 : 1;
          setTotalResults(totalCount);
          setTotalPages(pagesForCount);
          setFilteredResults(data);
        }
      );
    };

    fetchData();
  }, [page, filters]);

  const setFilter = (filterType, filterValue) => {
    switch (filterType) {
      case "tag":
        setFilters({
          ...filters,
          tags: {
            ...filters.tags,
            [filterValue]: !filters.tags[filterValue],
          },
        });
        break;
      case "subscription":
        setFilters({
          ...filters,
          subscription: !filters.subscription,
        });
        break;
      case "price":
        setFilters({
          ...filters,
          price: filterValue,
        });
        break;
      default:
        break;
    }
  };

  return [setFilter, filteredResults, totalPages, page, setPage, filters, totalResults];
};
