import React from "react";
import { Link } from 'react-router-dom';

export const HomePage = () => {
  return (
    <div className="App">
      <h1>Take home test</h1>
      <ul>
          <li><Link to="product-collection">Product collection</Link></li>
      </ul>
  </div>
  );
};
