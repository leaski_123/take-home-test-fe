import React from "react";
import "./styles.scss";
import { useFetchProducts } from "../../hooks";
import { ProductsCollectionResults } from "../../components/ProductsCollectionResults";
import { ProductsCollectionPagination } from "../../components/ProductsCollectionPagination";
import { ProductsCollectionFilters } from "../../components/ProductsCollectionFilters";

export const ProductCollectionPage = () => {
  const [
    setFilter,
    results,
    totalPages,
    currentPage,
    setPage,
    filters,
    totalResults,
  ] = useFetchProducts();

  return (
    <>
      <h1>Product Collection</h1>

      <div className="product-collection">
        <div className="product-collection__filters">
          <h2>Filters</h2>
          <ProductsCollectionFilters filters={filters} setFilter={setFilter} />
        </div>
        <div className="product-collection__results">
          <h2>Results</h2>
          <p>Total results : {totalResults}</p>
          <ProductsCollectionResults results={results} />
          <ProductsCollectionPagination
            totalPages={totalPages}
            currentPage={currentPage}
            setPage={setPage}
          />
        </div>
      </div>
    </>
  );
};
